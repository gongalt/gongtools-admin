import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';  
import { DataTableModule } from "angular2-datatable";
import { DropdownModule,ModalModule } from 'ng2-bootstrap';

// Services
import { AuthService } from '../providers/auth.service';

// Pipes
import { DataFilterPipe } from '../pipes/data-filter.pipe';
import { KeysFilterPipe } from '../pipes/keys-filter.pipe';

import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';

@NgModule({
  imports: [ 
    LoginRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DropdownModule.forRoot(),
    ModalModule.forRoot()  
  ],
  declarations: [
    LoginComponent,
    DataFilterPipe,
    KeysFilterPipe
  ],
  providers:[
    AuthService
  ],
  exports:[DataFilterPipe,KeysFilterPipe]
})
export class LoginModule { }
