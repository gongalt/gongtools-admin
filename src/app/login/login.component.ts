import { Component,OnInit } from '@angular/core';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
// Validator
import { emailValidator } from '../validators/validators';
// Services
import { AuthService } from '../providers/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public loginForm:FormGroup;
  public hasError:String;
  public auth_subscription:Subscription;

  constructor(public authServe: AuthService,public fb: FormBuilder,public router: Router) { }

  ngOnInit() {
    this.auth_subscription = this.authServe.auth.subscribe((auth)=>{
      if(auth){
        this.router.navigate(['/dashboard']);
      }
    });
    this.loginForm = this.fb.group({
	    email: ['', Validators.compose([Validators.required,  emailValidator])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  	});
  }
  
  public login(loginFormData) {
		console.log("Login Form Value:",loginFormData.value);
		if(loginFormData.valid){
			this.loginForm.value.email = loginFormData.value.email = _.toLower(loginFormData.value.email);
			this.authServe.login(loginFormData.value)
			  	.then((success) => this.onLoginSuccess(success))
			  	.catch((error) => this.onLoginError(error));
		}
	}

  private onLoginSuccess(success) : void{
    
    this.router.navigate(['/dashboard']);
  }
  
  private onLoginError(error): void{
    this.hasError = error.message;
    setTimeout(() => { this.hasError = ''; } , 3000);
  }

  ngOnDestroy(){
    this.auth_subscription.unsubscribe();
  }

}
