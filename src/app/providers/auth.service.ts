import { Injectable } from '@angular/core';
import {  AngularFireAuth, AngularFireDatabase, AngularFire } from 'angularfire2';

@Injectable()
export class AuthService {

  constructor(public db: AngularFireDatabase, public auth: AngularFireAuth,public af: AngularFire) {
		console.log('Hello AuthService Provider');
	}
  	login(loginForm){
		return this.auth.login(loginForm);
	}
	logout() {
		return this.af.auth.logout();
	}
	register(registerForm){
		return this.auth.createUser(registerForm);
	}
	registerProfile(uid,registerForm){
		return this.db.list('/users').update(uid,registerForm);
	}
}
