import { Injectable, Inject } from '@angular/core';
import { AngularFireDatabase, FirebaseApp } from 'angularfire2';
import * as firebase from 'firebase';
import * as moment from 'moment';

@Injectable()
export class VendorsService {
  firebase_ref = firebase.database().ref();
  storage_ref:any;
  img_name:any;
  constructor(public db: AngularFireDatabase,@Inject(FirebaseApp) public firebase_app: any) { 
    this.storage_ref = firebase_app.storage().ref();
  }

  getVendors(){
    return this.db.list('vendors');
  }
  getVendorPhotos(){
    return this.db.list('vendor_photos');
  }
  newVendor(vendor){
    return this.db.list('vendors').push(vendor);
  }

  setVendor(uid,vendor){
    return this.db.list('vendors').update(uid,vendor);
  }
  
  updateVendorPhoto(vendor_id,vendor_photo_id){
    let updated_data = {};
    updated_data[vendor_photo_id] = true;
    return this.db.object('vendors/' + vendor_id + '/photos').update(updated_data);
  }

  removeVendor(uid){
    return this.db.list('vendors/' + uid).remove();
  }
  removeVendorPhoto(vendor_photo_id){
    return  this.db.list('vendor_photos/'+vendor_photo_id).remove();
  }
  removeImage(image_name){
    console.log('delete vendor_photos/'+image_name);
    return this.storage_ref.child('vendor_photos/'+image_name).delete();
  }
  storeImage(uid,image){
    let image_name = uid + image.name.replace(/[^A-Z0-9]+/ig, "_");
    return this.storage_ref.child('vendor_photos/'+ image_name).put(image);
  }

  setImage(image_name,image_url){
    return this.db.list('vendor_photos/').push({name:image_name,image:image_url});
  }
  updateImage(uid,image_name,image_url){
    return this.db.object('vendor_photos/'+uid).update({name:image_name,image:image_url});
  }
  getImage(image){
    return this.storage_ref.child('vendor_photos/'+image).getDownloadURL();
  }

}
