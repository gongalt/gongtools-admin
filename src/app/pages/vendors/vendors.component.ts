import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';

import * as _ from 'lodash';
// Validator
import { emailValidator, matchingPasswords } from '../../validators/validators';

// Services
import { AuthService } from '../../providers/auth.service';
import { VendorsService } from './vendors.service';
import { RecommendedService } from '../recommended-vendors/recommended.service';


@Component({
  selector: 'app-vendors',
  templateUrl: './vendors.component.html',
  styleUrls: ['./vendors.component.scss']
})
export class VendorsComponent implements OnInit {
  @ViewChild('addVendorsModal') addVendorsModal: ModalDirective;
  @ViewChild('updateVendorsModal') updateVendorsModal: ModalDirective;
  @ViewChild('deleteVendorsModal') deleteVendorsModal: ModalDirective;
  @ViewChild('updateForm') updateForm;
  public filterQuery = "";
  public rowsOnPage = 5;
  public sortBy = "email";
  public sortOrder = "asc";
  
  public error_message:String;
  public success_message:String;

  public auth_subscription:Subscription;
  
  public data:any;
  public all_vendors_subscription:Subscription;
  public vendor_photos_subscription:Subscription;
  public all_recommended_subscription:Subscription;

  public add_vendor_form:FormGroup;
  public update_vendor_form:FormGroup;

  public category_list:any;
  public vendor_selected:any;
  public vendor_photo_list:any;
  public vendor_image_selected:any;
  public images:any;
  public error_ext:boolean;
  public error_size:boolean;
  public show_loader:boolean;
  public progress:any;
  public counter:number = 1;
  public add_photo:boolean;
  public update_vendor_add_photo:boolean;
  public category_empty:boolean = false;;
  public new_category:boolean = false;
  public recommended_vendors:Array<any>;
  constructor(
  public router:Router,
  public authServe: AuthService,
  public vendorsServe:VendorsService,
  public fb:FormBuilder,
  public recommendedServe: RecommendedService) { }

  ngOnInit() {
    this.auth_subscription = this.authServe.auth.subscribe((auth)=>{
      if(!auth){
        this.router.navigate(['/login']);
      }
    });
    this.all_vendors_subscription = this.vendorsServe.getVendors().subscribe((vendors)=>{
      this.data = vendors;
      this.getVendorCategories();
      this.vendorPhotos();
    });
    this.all_recommended_subscription  = this.recommendedServe.getRecommended().subscribe((recommended)=>{
      this.recommended_vendors = recommended;
    });
    this.forms();
  }
  ngOnDestroy(){
    this.auth_subscription.unsubscribe();
    this.all_vendors_subscription.unsubscribe();
    this.vendor_photos_subscription.unsubscribe();
  }
  public forms(){
    this.add_vendor_form = this.fb.group({
      email: ['', Validators.compose([Validators.required,  emailValidator])],
      category: ['',Validators.required],
      en_address: ['',Validators.required],
      en_best_transport: ['',Validators.required],
      en_business_name: ['',Validators.required],
      en_description: ['',Validators.required],
      ja_address: ['',Validators.required],
      ja_best_transport: ['',Validators.required],
      ja_business_name: ['',Validators.required],
      ja_description: ['',Validators.required],
      ja_category: ['',Validators.required],
      japanese_menu: ['',Validators.required],
      speaks_japanese: ['',Validators.required],
      telephone: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      website: ['',Validators.required],
      wifi: ['',Validators.required],
    });
    
    this.update_vendor_form = this.fb.group({
      $key: [""],
      email: ['', Validators.compose([Validators.required,  emailValidator])],
      category: ['',Validators.required],
      en_address: ['',Validators.required],
      en_best_transport: ['',Validators.required],
      en_business_name: ['',Validators.required],
      en_description: ['',Validators.required],
      ja_address: ['',Validators.required],
      ja_best_transport: ['',Validators.required],
      ja_business_name: ['',Validators.required],
      ja_description: ['',Validators.required],
      ja_category: ['',Validators.required],
      japanese_menu: ['',Validators.required],
      speaks_japanese: ['',Validators.required],
      telephone: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      website: ['',Validators.required],
      wifi: ['',Validators.required],
    });

  }

  public updateForms(data){
    this.update_vendor_form.patchValue({
      $exists: function () {},
      $key: data.$key,
      email: data.email,
      category: data.category,
      en_address: data.en.address,
      en_best_transport: data.en.best_transport,
      en_business_name: data.en.business_name,
      en_description: data.en.description,
      ja_address: data.ja.address,
      ja_best_transport: data.ja.best_transport,
      ja_business_name: data.ja.business_name,
      ja_description: data.ja.description,
      ja_category: data.ja.category,
      japanese_menu: data.japanese_menu,
      speaks_japanese: data.speaks_japanese,
      telephone: data.telephone,
      website: data.website,
      wifi: data.wifi,
    });
  }

  public getVendorCategories(){
    let categories = new Set();
    _.some(this.data,(value,key)=>{
      categories.add(value.category);
    });
    console.log("Categories:",Array.from(categories));
    this.category_list = Array.from(categories);
    if(_.isEmpty(this.category_list)){
      this.category_empty = true;
    }
  }
  public vendorPhotos(){
    this.vendor_photos_subscription = this.vendorsServe.getVendorPhotos().subscribe((photos)=>{
      this.vendor_photo_list = photos;
      console.log("this.vendor_photo_list",this.vendor_photo_list);
    });
  }
  public showModal(theModal): void {
    this[theModal].show();
  }

  public hideModal(theModal): void {
    this[theModal].hide();
  }
  deleteVendor(vendor_data){
    console.log("Delete:",vendor_data);
    console.log("recommended_vendors:",_.find(this.recommended_vendors,{vendor_id:vendor_data.$key}));
    let recommended_id:any = (_.find(this.recommended_vendors,{vendor_id:vendor_data.$key}) ? _.find(this.recommended_vendors,{vendor_id:vendor_data.$key}).$key : null);
    this.vendorsServe.removeVendor(vendor_data.$key).then((success)=>{
      console.log("Successfully Deleted!",success);
      if(!_.isEmpty(recommended_id)){
        this.recommendedServe.removeVendor(recommended_id);
      }
    }).catch((error)=>{
      console.log("Error:",error.message);
    })
  }
  setVendor(vendor_data,type){
    console.log("Vendor Data:",vendor_data.value);
    if(vendor_data.valid){
      this.show_loader = true;
      switch(type){
        case "add":
          vendor_data.value.category = _.toLower(vendor_data.value.category);
          vendor_data = this.makeSubObject(vendor_data,this.propertyArray());
          this.vendorsServe.newVendor(vendor_data.value).then((new_vendor)=>{
            console.log("new_vendor:",new_vendor.key);
            if(!_.isEmpty(this.images)){
              let images_counter = _.size(this.images);
              console.log("Image Counter:",images_counter);
              _.some(this.images,(image)=>{
                this.uploadTask(new_vendor.key,image,'add')
                this.add_photo = true;
              });
            }else{
              this.add_vendor_form.reset();
              this.success_message = "Successfully added vendor!";
              this.show_loader = false;
              setTimeout(()=>{
                this.success_message = '';
                this.hideModal('addVendorsModal');
              },2000);
            }
            
          }).catch((error)=>{
            console.log("error:",error.message);
            this.error_message = error.message;
            setTimeout(()=>{
              this.error_message = '';
            },2000);
          });
          break;
        case "update":
          vendor_data = this.makeSubObject(vendor_data,this.propertyArray());
          let uid = vendor_data.value.$key;
          delete vendor_data.value.$key;
          this.vendorsServe.setVendor(uid,vendor_data.value).then(()=>{
            console.log("this.images update:",this.images);
            if(!_.isEmpty(this.images)){
              let images_counter = _.size(this.images);
              console.log("Image Counter:",images_counter);
              _.some(this.images,(image)=>{
                this.uploadTask(uid,image,'add')
                this.update_vendor_add_photo = true;
              });
            }else{
              this.update_vendor_form.reset();
              this.success_message = "Successfully updated vendor!";
              this.show_loader = false;
              setTimeout(()=>{
                this.success_message = '';
                this.hideModal('updateVendorsModal');
              },2000);
            }
          }).catch((error)=>{
            console.log("error:",error.message);
            this.error_message = error.message;
            this.show_loader = false;
            setTimeout(()=>{
              this.error_message = '';
            },2000);
          });
          break;
        default:
          break;
      }
    }
  }
  private propertyArray(){
    let array:Array<any> = ['en_business_name',
        'en_address',
        'en_best_transport',
        'en_description',
        'ja_business_name',
        'ja_address',
        'ja_best_transport',
        'ja_description',
        'ja_category'];
    return array;
  }
  private makeSubObject(formName,property_array){
    _.some(property_array,(property,counter)=>{
      let x = property.split('_');
      let new_property;
      if(!_.isEmpty(x[2])){
        new_property = x[1] + '_' + x[2];
      }else{
        new_property = x[1];
      }
      if(typeof formName.value[x[0]] !== 'object'){
        formName.value[x[0]] = {};
      }else if((!_.isEmpty(formName.value[x[0]])) && counter == 4){
        formName.value[x[0]] = {};
      }
      formName.value[x[0]][new_property] = formName.value[property];
      delete formName.value[property];
    });
      console.log("Form:",formName.value);
      return formName;
  }
  getVendorImages(vendor_photo_keys){
    let photos = new Set();
    _.some(vendor_photo_keys,(value,key)=>{
      photos.add(_.find(this.vendor_photo_list,{$key:key}));
    });
    return Array.from(photos);
  }
  onChange(event: any,type:any) {
    let files = [].slice.call(event.target.files);
    let accepted = ['jpg','jpeg','JPG','JPEG','png','PNG','gif','GIF'];
    let imgs = new Set();
    let error_type:boolean;
    let error_size:boolean;
    console.log("Files",files);
    _.some(files,(file)=>{
      console.log("file",file);
      let ext = file.name.split('.').pop();
      if(_.includes(accepted,ext)){
        if(file.size < (1024 * 1024 * 2)){
          imgs.add(file);
        }else{
          error_size = true;
        }
      }else{
        error_type = true;
      }
    })
    this.error_ext = error_type;
    this.error_size = error_size;
    if(!error_type && !error_size){
      this.images = Array.from(imgs);
      if(type == 'update'){
        this.updateImage();
      }
    }
    console.log("This.images",this.images);
    console.log("Files",files);
  }
  updateImage(){
    _.some(this.images,(image)=>{
      this.uploadTask(this.vendor_selected.$key,image,'update')
    });
  }
  imgOnError(event:any, action):void {
    this[action] = "http://focusyouronlinemarketing.com/heating/wp-content/uploads/2013/12/default-placeholder-1024x1024-200x200.png";
  }
  uploadTask(vendor_key,image,type){
    console.log("image",image);
    console.log("vendor_key",vendor_key);
    console.log("type",type);
    let uploadTask = this.vendorsServe.storeImage(vendor_key,image);
    uploadTask.on('state_changed',(snapshot)=>{
      let progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      this.progress = progress;
      if(progress == 100){
        this.counter++;
        console.log("current counter",this.counter);
        this.progress = "";
      }
    },(error)=>{

    },(success)=>{
      let image_name = vendor_key + image.name.replace(/[^A-Z0-9]+/ig, "_");
      let image_url = uploadTask.snapshot.downloadURL;
      switch(type){
        case 'add':
          this.vendorsServe.setImage(image_name,image_url).then((vendor_image)=>{
            let image_key = vendor_image.key;
            this.updateVendorPhotos(vendor_key,image_key);
          });
          break;
        case 'update':
            this.vendorsServe.removeImage(this.vendor_image_selected.name);
            this.vendorsServe.updateImage(this.vendor_image_selected.$key,image_name,image_url);
          break;
      }
      
    });
  }
  updateVendorPhotos(vendor_key,image_key){
    this.vendorsServe.updateVendorPhoto(vendor_key,image_key).then((vendor_photos)=>{
      console.log("updated vendor photos:",vendor_photos);
    }).catch((error)=>{
      console.log("update vendor photos error:",error.message);
    });
  }
  ngDoCheck(){
    if(this.add_photo){
      let images_counter = _.size(this.images);
      if(this.counter == images_counter){
        this.add_vendor_form.reset();
        this.images = "";
        this.success_message = "Successfully added vendor!";
        this.show_loader = false;
        setTimeout(()=>{
          this.success_message = '';
          this.hideModal('addVendorsModal');
          this.counter = 1;
          this.add_photo = false;
        },2000);
      }
    }
    
    if(this.update_vendor_add_photo){
      let images_counter = _.size(this.images);
      if(this.counter == images_counter){
        this.update_vendor_form.reset();
        this.images = "";
        this.success_message = "Successfully added vendor!";
        this.show_loader = false;
        setTimeout(()=>{
          this.success_message = '';
          this.hideModal('updateVendorsModal');
          this.counter = 1;
          this.update_vendor_add_photo = false;
          this.updateForm.nativeElement.reset();
        },2000);
      }
    }
    
  }
}
