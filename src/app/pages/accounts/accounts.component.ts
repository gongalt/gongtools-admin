import { Component, OnInit, ViewChild,Input } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
// Validator
import { emailValidator, matchingPasswords } from '../../validators/validators';
// Services
import { AuthService } from '../../providers/auth.service';
import { AccountsService } from './accounts.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  @ViewChild('addAccountsModal') addAccountsModal: ModalDirective;
  @ViewChild('updateAccountModal') updateAccountModal: ModalDirective;
  @ViewChild('deleteAccountModal') deleteAccountModal: ModalDirective;
  
  public filterQuery = "";
  public rowsOnPage = 10;
  public sortBy = "email";
  public sortOrder = "asc";
  public data:any;

  public error_message:String;
  public success_message:String;
  
  public add_accounts_form: FormGroup;
  public update_accounts_form:FormGroup;

  public all_accounts:Array<any> = [];
  public all_accounts_subscription: Subscription;
  public accounts_favorites:Array<any>;
  public accounts_favorites_subscription:any;
  
  public all_vendors:Array<any>;
  public all_vendors_subscription:Subscription;
  public favorites:String;
  
  public account_selected:any;
  
  constructor(public router:Router,
    public fb:FormBuilder,
    public authServe: AuthService,
    public accountsServe: AccountsService) { }

  ngOnInit() {
    this.add_accounts_form = this.fb.group({
      email: ['', Validators.compose([Validators.required,  emailValidator])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      cpassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    }, {validator: matchingPasswords('password', 'cpassword')});
    this.update_accounts_form = this.fb.group({
      $key: [""],
      email: ["", Validators.compose([Validators.required,  emailValidator])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      cpassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    }, {validator: matchingPasswords('password', 'cpassword')});

    this.getAccounts();
    this.getVendors();
  }
  public showModal(theModal): void {
    this[theModal].show();
  }

  public hideModal(theModal): void {
    this[theModal].hide();
  }
  public refresh(event:any, refresh:any): void {
    this[refresh] = event.id;
  }

  public selected(event:any, selected:any): void {
    if(selected === "Accounts_selected" || selected === "Accounts_update_selected") {
      this[selected] = _.concat(_.remove(this[selected], undefined),event.id);
    } else {
      this[selected] = event.id;
    }
  }

  public removed(event, action, container): void {
    this[action] = event;
    _.remove(this[container], (value) => {
      return value === event.id
    });
  }

  public addAccount(add_account_data){
    console.log("Add Account:",add_account_data.value);
    if(add_account_data.valid){
      add_account_data.value.email = _.toLower(add_account_data.value.email);
      let account = {
        email: add_account_data.value.email,
        password: add_account_data.value.password
      }
      this.authServe.register(account).then((success)=>{
        this.authServe.registerProfile(success.uid,{email:add_account_data.value.email}).then((successProfile)=>{
          this.success_message = "Account successfully created!";
          setTimeout(()=>{ this.success_message = '';},5000);
        }).catch((error)=>{
          this.error_message = error.message;
          setTimeout(()=>{ this.error_message = '';},5000);
        });
      }).catch((error)=>{
        this.error_message = error.message;
        setTimeout(()=>{ this.error_message = '';},5000);
      });
    }
  }

  public getAccounts(){
    this.all_accounts_subscription = this.accountsServe.getAccountsList().subscribe((accounts)=>{
      this.data = accounts;
    });
  }

  public getVendors(){
    this.all_vendors_subscription = this.accountsServe.getVendors().subscribe((vendors)=>{
      this.all_vendors = vendors;
    });
  }

  public getFavorites(key){
    this.favorites = _.find(this.all_vendors, {'$key': key});
  }
  
  public refreshArray(){
    this.favorites = "";
  }

  public updateAccountPassword(data){
    console.log("Update:",data);   
    this.accountsServe.setNewPassword(data.email).then((success)=>{
      console.log("Success:",success);
    }).catch((error)=>{
      console.log("Error:",error.message);
    });
  }

  public deleteAccount(data){
    console.log("Delete:",data);
  }  

  ngOnDestroy(){
    this.all_accounts_subscription.unsubscribe();
    this.all_vendors_subscription.unsubscribe();
  }

 
}
