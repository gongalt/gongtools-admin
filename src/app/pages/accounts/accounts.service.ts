import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2';
import * as firebase from 'firebase';

@Injectable()
export class AccountsService {
  firebase_ref = firebase.database().ref();
  constructor(public db: AngularFireDatabase) { }
  
  getAccountsList(){
    return this.db.list('/users');
  }

  getFavorites(favorites_id){
    return this.db.object('vendors/' + favorites_id);
    // let query_favorites = this.firebase_ref.child('vendors').orderByKey().equalTo(favorites_id);
    // return query_favorites.once('value',(snap)=>{ snap.val(); });
  }

  getVendors(){
    return this.db.list('/vendors');
  }

  setNewPassword(email){
    return firebase.auth().sendPasswordResetEmail(email);
  }
}
