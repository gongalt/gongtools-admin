import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';  
import { DataTableModule } from "angular2-datatable";
import { DropdownModule,ModalModule,TabsModule } from 'ng2-bootstrap';
// Services
import { AuthService } from '../providers/auth.service';
import { AccountsService } from './accounts/accounts.service';
import { VendorsService } from './vendors/vendors.service';
import { RecommendedService } from './recommended-vendors/recommended.service';

import { AccountsComponent } from './accounts/accounts.component';
import { RecommendedVendorsComponent } from './recommended-vendors/recommended-vendors.component';
import { VendorsComponent } from './vendors/vendors.component';

import { PagesRoutingModule } from './pages-routing.module';
import { LoginModule } from '../login/login.module';



@NgModule({
  imports: [ 
    PagesRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DropdownModule.forRoot(),
    ModalModule.forRoot(),
    LoginModule,
    TabsModule.forRoot() 
  ],
  declarations: [
    AccountsComponent,
    RecommendedVendorsComponent,
    VendorsComponent
  ],
  providers:[
    AuthService,
    AccountsService,
    VendorsService,
    RecommendedService
  ]
})
export class PagesModule { }
