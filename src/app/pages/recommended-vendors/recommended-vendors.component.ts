import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ng2-bootstrap';
import { Router } from '@angular/router';
import * as _ from 'lodash';
import { FormBuilder, Validators,FormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
// Services
import { AuthService } from '../../providers/auth.service';
import { VendorsService } from '../vendors/vendors.service';
import { RecommendedService } from './recommended.service';


@Component({
  selector: 'app-recommended-vendors',
  templateUrl: './recommended-vendors.component.html',
  styleUrls: ['./recommended-vendors.component.scss']
})
export class RecommendedVendorsComponent implements OnInit {
  @ViewChild('addRecommendedVendorsModal') addRecommendedVendorsModal: ModalDirective;
  @ViewChild('updateRecommendedVendorsModal') updateRecommendedVendorsModal: ModalDirective;
  @ViewChild('deleteRecommendedVendorsModal') deleteRecommendedVendorsModal: ModalDirective;
  public filterQuery = "";
  public rowsOnPage =5;
  public sortBy = "email";
  public sortOrder = "asc";
  
  public error_message:String;
  public success_message:String;

  public auth_subscription:Subscription;
  
  public data:any;
  public all_vendors_subscription:Subscription;
  public all_recommended_subscription:Subscription;
    

  public add_recommended_form:FormGroup;
  public update_recommended_form:FormGroup;

  public category_list:any;
  public vendor_selected:any;
  
  public all_vendors:any;
  public all_vendors_keys:any;
  public counter:Array<any> = [];
  public recommended_list:any;
  public is_empty:boolean = false;
  constructor(public router:Router,public authServe: AuthService,public vendorsServe:VendorsService,public recommendedServe: RecommendedService,public fb:FormBuilder) { }

  ngOnInit() {
    this.auth_subscription = this.authServe.auth.subscribe((auth)=>{
      if(!auth){
        this.router.navigate(['/login']);
      }
    });
    this.all_recommended_subscription  = this.recommendedServe.getRecommended().subscribe((recommended)=>{
      this.data = recommended;
    });
    this.all_vendors_subscription = this.vendorsServe.getVendors().subscribe((vendors)=>{
      this.all_vendors = vendors;
      this.getVendorCategories();
      this.recommendedList();
    });
    this.forms();
    
  }
  ngOnDestroy(){
    this.auth_subscription.unsubscribe();
    this.all_recommended_subscription.unsubscribe();
    this.all_vendors_subscription.unsubscribe();
  }
  public forms(){
    this.add_recommended_form = this.fb.group({
      category: ['',Validators.required],
      speaks_japanese: ['',Validators.required],
      vendor_id: ['',Validators.required],
      ratings: ['',Validators.required]
      
    });
    
    this.update_recommended_form = this.fb.group({
      $key: [""],
      category: ['',Validators.required],
      speaks_japanese: ['',Validators.required],
      vendor_id: ['',Validators.required],
      ratings: ['',Validators.required]
    });

  }

  public updateForms(data){
    this.update_recommended_form.patchValue({
      $exists: function () {},
      $key: data.$key,
      category: data.category,
      speaks_japanese: data.speaks_japanese,
      vendor_id: data.vendor_id,
      ratings: data.ratings
    });
  }

  public getVendorCategories(){
    let categories = new Set();
    _.some(this.all_vendors,(value)=>{
      categories.add(value.category);
    });
    console.log("Categories:",Array.from(categories));
    this.category_list = Array.from(categories);
  }
  public recommendedList(){
    let recommended = new Set();
    _.some(this.all_vendors,(value)=>{
      recommended.add(value.$key);
    });
    _.some(this.data,(rvalue)=>{
      recommended.delete(rvalue.vendor_id);
    });
    console.log("Recommended:",Array.from(recommended));
    this.recommended_list = Array.from(recommended);
    (_.isEmpty(this.recommended_list) ? this.is_empty = true: this.is_empty = false);
  }
  public showModal(theModal): void {
    this[theModal].show();
  }

  public hideModal(theModal): void {
    this[theModal].hide();
  }
  deleteRecommendedVendor(vendor_data){
    console.log("Delete:",vendor_data);
    this.recommendedServe.removeVendor(vendor_data.$key).then((success)=>{
      console.log("Successfully Deleted!",success);
      this.recommendedList();
    }).catch((error)=>{
      console.log("Error:",error.message);
    })
  }
  setRecommendedVendor(vendor_data,type){
    console.log("Vendor Data:",vendor_data.value);
    if(vendor_data.valid){
      switch(type){
        case "add":
          this.recommendedServe.newVendor(vendor_data.value).then(()=>{
            this.add_recommended_form.reset();
            this.success_message = "Successfully added vendor!";
            this.recommendedList();
            setTimeout(()=>{
              this.success_message = '';
              this.hideModal('addRecommendedVendorsModal');
            },2000);
          }).catch((error)=>{
            console.log("error:",error.message);
            this.error_message = error.message;
            setTimeout(()=>{
              this.error_message = '';
            },2000);
          });
          break;
        case "update":
          let uid = vendor_data.value.$key;
          delete vendor_data.value.$key;
          this.recommendedServe.setVendor(uid,vendor_data.value).then(()=>{
            this.add_recommended_form.reset();
            this.success_message = "Successfully updated vendor!";
            setTimeout(()=>{
              this.success_message = '';
              this.hideModal('updateRecommendedVendorsModal');
            },2000);
          }).catch((error)=>{
            console.log("error:",error.message);
            this.error_message = error.message;
            setTimeout(()=>{
              this.error_message = '';
            },2000);
          });
          break;
        default:
          break;
      }
    }
  }
  getName(list, objID): string {
    let obj:any = _.find(this[list], {$key: objID});
    return obj ? obj.en.business_name : '';
  }

  setRatingCounter(rating){
    let arr:Array<number> = [];
    for(let i = 0; i < rating; i++){
      arr.push(0);
    }
    return this.counter = arr;
  }
  refresh(){
    this.counter = [];
  }
}
