import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2';
import * as firebase from 'firebase';

@Injectable()
export class RecommendedService {
  firebase_ref = firebase.database().ref();
  constructor(public db: AngularFireDatabase) { }

  getRecommended(){
    return this.db.list('recommended');
  }
  
  newVendor(vendor){
    return this.db.list('recommended').push(vendor);
  }

  setVendor(uid,vendor){
    return this.db.list('recommended').update(uid,vendor);
  }

  removeVendor(uid){
    return this.db.list('recommended/' + uid).remove();
  }
}
