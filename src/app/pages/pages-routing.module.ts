import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AccountsComponent } from './accounts/accounts.component';
import { RecommendedVendorsComponent } from './recommended-vendors/recommended-vendors.component';
import { VendorsComponent } from './vendors/vendors.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Example Pages'
    },
    children: [
      {
        path: 'accounts',
        component: AccountsComponent,
        data: {
          title: 'Accounts Page'
        }
      },
      {
        path: 'recommended-vendors',
        component: RecommendedVendorsComponent,
        data: {
          title: 'Recommended Vendors'
        }
      },
      {
        path: 'vendors',
        component: VendorsComponent,
        data: {
          title: 'Vendors Page'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {}
