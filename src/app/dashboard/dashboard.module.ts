import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule  }   from '@angular/forms';  
import { DataTableModule } from "angular2-datatable";
import { DropdownModule,ModalModule } from 'ng2-bootstrap';
import { LoginModule } from '../login/login.module';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
// Services
import { AuthService } from '../providers/auth.service';

@NgModule({
  imports: [
    DashboardRoutingModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    DataTableModule,
    DropdownModule.forRoot(),
    ModalModule.forRoot(),
    LoginModule,
    CommonModule
  ],
  declarations: [ DashboardComponent ],
  providers: [
    AuthService
  ]
})
export class DashboardModule { }
